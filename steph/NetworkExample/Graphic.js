
// socket example from:
// https://gist.github.com/tedmiston/5935757
// https://gist.github.com/creationix/707146

// install: npm install ohm-js
var ohm = require('ohm-js');

net = require('net');

var contents = `
Graphic {

Graphic = Shape+

number = digit+

blue = "blue"
color = blue

Point = "(" number "," number ")"

// defined via top left and bottom right points
Rect = "rect:" Point Point color?

diameter = number
Circle = "circle:" Point diameter

Shape = Rect | Circle
 
}`

// later localhost
// later exec JS code
var g = ohm.grammar(contents);

// Haskell Rasterific code:
var semantics =
    g.createSemantics().addOperation('eval', {	
        Graphic: function(shapes) {
	    return shapes.eval();
	},
	Shape: function(a) {
            return a.eval();
	},
	Circle: function(terminal, point, diameter) {
            const part1 = "  (texture (PixelRGBA8 0 0x86 0xc1 255)) $ fill $ ";
	    return (part1 + "circle " + point.eval() + diameter.eval());
	},
	Point: function(unused1, x, unused2, y, unused3) {
	    return ("(V2 " + x.sourceString + " " + y.sourceString + ") ")
	},	
	diameter: function(number) {
	    return number.sourceString;
	},
	/*number: function(digits) {
	    return ("[a number]");
	},*/
	// for debugging:
    	_terminal: function() {
	    return "generic _terminal reached";
	},
        // for debugging:
	_nonterminal: function(a) {
	    return ("generic _nonterminal reached '" + a.sourceString +"'");
	} 
    });

net.createServer(function (socket) {
  
  socket.name = socket.remoteAddress + ":" + socket.remotePort   
  socket.write("Welcome " + socket.name + "\n");  

  socket.on('data', function (data) {    
    console.log("===DATA: " + data);
    
    var result="";
    result += "start. ";    
    var m = g.match(data);

    if (m.succeeded()) {
       result += "===\n succeeded: \n";  
       result += semantics(m).eval();
       var match = semantics(m).eval();
 
       var client = new net.Socket();
       console.log('trying to connect...');

       client.connect(4000, '127.0.0.1', function() {
	console.log('Connected');
	client.write("" + match + "\n");
       });
       client.on('data', function(data) {
	  console.log('Received: ' + data);
	  client.destroy(); // kill client after server's response
       });

       client.on('close', function() {
	  console.log('Connection closed');
       });

       
    } else {
      result += "error";
      result = m.message;  // Extract the error message.
    }
    console.log(result);

  });

  // Remove the client from the list when it leaves
  socket.on('end', function () {
    console.log("client goes away");
  });
}).listen(3000);

/*
var result="";
result += "start. ";
var match="";

var m = g.match(`
circle: (50, 50) 30
`);

if (m.succeeded()) {
    result += "===\n succeeded: \n";  
    result += semantics(m).eval();
    match = semantics(m).eval();
    result += "===\n now executing: \n";  
    //result += eval("" + semantics(m).eval() );
} else {
  result += "error";
  result = m.message;  // Extract the error message.
}
console.log(result);
*/

