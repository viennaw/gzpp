import Codec.Picture( PixelRGBA8( .. ), writePng )
import Graphics.Rasterific
import Graphics.Rasterific.Texture

white = PixelRGBA8 255 255 255 255

texture :: px -> Drawing px () -> Drawing px ()
texture = (\x -> withTexture (uniformTexture x))

fileName = "GeneratedImage.png"

main :: IO ()
main = writePng fileName (renderDrawing 900 600 white $ drawing)

drawing :: Drawing PixelRGBA8 ()
drawing = do
  (texture white) $ fill $ circle (V2 0 0) 0 -- dummy                       

  (texture (PixelRGBA8 0 0x86 0xc1 255)) $ fill $ circle (V2 10 10) 40
