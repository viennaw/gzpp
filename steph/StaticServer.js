// npm install express
var express = require('express');
var app = express();

//setting middleware
// __dirname + 'public'
app.use(express.static(__dirname)); //Serves resources from public folder

var server = app.listen(5000);
