
// install: npm install ohm-js
var ohm = require('ohm-js');

var contents = `
Graphic {

Graphic = Shape+

number = digit+

blue = "blue"
color = blue

Point = "(" number "," number ")"

// defined via top left and bottom right points
Rect = "rect:" Point Point color?

diameter = number
Circle = "circle:" Point diameter

Shape = Rect | Circle
 
}`

// later localhost
// later exec JS code
var g = ohm.grammar(contents);

var semantics =
    g.createSemantics().addOperation('eval', {	
        Graphic: function(shapes) {
	    return shapes.eval();
	},
	Circle: function(terminal, point, diameter) {
	    return ("circle " + point + diameter);
	},
	diameter: function(number) {
	    return ("[a diameter]");
	},
	number: function(digits) {
	    return ("[a number]");
	},
	// for debugging:
    	_terminal: function() {
	    return "generic _terminal reached";
	},
        // for debugging:
	_nonterminal: function(a) {
	    return ("generic _nonterminal reached '" + a.sourceString +"'");
	} 
    });

var result="";
result += "start. ";
var match="";

var m = g.match(`
circle: (50, 50) 30
`);

if (m.succeeded()) {
    result += "===\n succeeded: \n";  
    result += semantics(m).eval();
    match = semantics(m).eval();
    result += "===\n now executing: \n";  
    //result += eval("" + semantics(m).eval() );
} else {
  result += "error";
  result = m.message;  // Extract the error message.
}

console.log(result);


