
// install: npm install ohm-js
var ohm = require('ohm-js');

var contents = `
NetLang {

NetLang = RightArrow | LeftArrow

p = digit digit digit digit
Port = "(" "port" p ")"

String = "'" (~"'" any)+ "'"

RightArrow = Port "-->" "to-console"
LeftArrow = Port "<--" String
      
}
`

// later localhost
// later exec JS code
var g = ohm.grammar(contents);

var semantics =
    g.createSemantics().addOperation('eval', {
	NetLang: function(a) {
	    return a.eval();
	},
	RightArrow: function(port, arrow, toConsole) {
	    const p = port.eval();

	    // convert arrow command to bash netcat 
	    return "netcat -lk " + p
	},
	LeftArrow: function(port, arrow, str) {
	    const p = port.eval();
	    const s = str.eval();

	    // convert arrow command to bash netcat 
	    return ("echo \"" + s + "\" | netcat -q0 127.0.0.1 " + p);
	},
	String: function(unused, str, unused) {
	    return ("" + str.sourceString);
	},
	Port: function(unused, unused, digits, unused) {
	    return ("" + digits.sourceString);	    
	},
	// for debugging:
    	_terminal: function() {
	    return "generic _terminal reached";
	},
	_nonterminal: function(a) {
	    return "generic _nonterminal reached";
	}
    });

var result="";
result += "start. ";
var match="";

var m = g.match(`
(port 3000) <-- 'hello there'
(port 3000) <-- 'something else to say.'
`);

if (m.succeeded()) {
    result += "===\n succeeded: \n";  
    result += semantics(m).eval();
    match = semantics(m).eval();
    result += "===\n now executing: \n";  
    //result += eval("" + semantics(m).eval() );
} else {
  result += "error";
  result = m.message;  // Extract the error message.
}

console.log(result);

var str = largeString.split("\n");


// execute as bash:
var exec = require('child_process').exec;
function puts(error, stdout, stderr) { console.log(stdout) }
exec("ls -la", puts);
