
// install: npm install ohm-js
var ohm = require('ohm-js');

var contents = `
html {
  
  html = tagP html   -- tagP
        | tag html   -- tag
        | any html   -- any
        | end        -- end
  
  tag = ~tagP "<" any* ">"
     
  tagP = "<p>" 
      
}
`
var g = ohm.grammar(contents);

var semantics = g.createSemantics().addOperation('eval', {
  html_tagP: function(unused, html) {
    return "1 + " + html.eval();
  },
  html_any: function(unused, html) {
    return html.eval();
  },
  html_end: function(unused) {
    return "0";
  }
  
});
var result="";
result += "start. ";
var m = g.match('<p> hello world </p> <apa> <p> nice! </p> sdfjejfdc <h></h>');
if (m.succeeded()) {
  result += "succeeded:";  
  result += semantics(m).eval();
  result += " = "
  result += eval("" + semantics(m).eval() );
} else {
  result += "error";
  result = m.message;  // Extract the error message.
}

console.log(result);
